package com.zuehlke.carrera.javapilot.akka.library;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;

public class PoolThread extends Thread{
    private BlockingQueue<Runnable> taskQueue;
    private boolean isStopped;
    private static Logger logger = LoggerFactory.getLogger(PoolThread.class);

    public PoolThread(BlockingQueue<Runnable> queue){
        taskQueue = queue;
        isStopped = false;
    }
    @Override
    public void run(){
        while(!isStopped){
            try {
                Runnable runnable = taskQueue.take();
                runnable.run();
            } catch (InterruptedException e) {
                logger.error("Thread was interrupted: " + e.getMessage());
            }
        }
    }
    public synchronized void doStop(){
        isStopped = true;
        this.interrupt();
    }
    public synchronized boolean isStopped(){
        return isStopped;
    }
}
