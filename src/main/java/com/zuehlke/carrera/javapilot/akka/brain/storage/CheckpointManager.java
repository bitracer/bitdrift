package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CheckpointManager {

    private static Map<String, ArrayList<ConfigCheckpoint>> checkpoints = new HashMap<>();
    public static int activeCheckpointNr;
    private static String activeCheckpointType;
    private static Timestamp activeCheckpointActivationTime;
    private static boolean allStable = false;
    private static ArrayList<Long> originalCheckpointDeltaMS;
    public static boolean recognizingCheckpoints = false;
    public static ArrayList<Long> recognizedCheckpoints = new ArrayList<>();

    public static void saveOriginalCheckpointDeltaData (ArrayList<Long> data) {
        originalCheckpointDeltaMS = data;
    }

    public static boolean checkIfStable(){
        if(allStable) return true;
        ArrayList<ConfigCheckpoint>cp = checkpoints.get(Options.VELOCITY_CHECKPOINTS);
        for(ConfigCheckpoint c: cp) {
            if(c.getModificationCounter() < 2){
                return false;
            }
        }
        System.out.println("All stable now");
        return (allStable = true);
    }
    public static long getTimePassedSinceActiveCheckpoint() {
        return timePassedSinceActiveCheckpoint;
    }
    private static long timePassedSinceActiveCheckpoint = 0;

    public static void register(String type) {
        checkpoints.put(type, new ArrayList<>());
    }

    public static void add(String type, ConfigCheckpoint checkpoint) {
        checkpoints.get(type).add(checkpoint);
    }

    public static void updateCheckpointRecognition (Timestamp activationTime) {

        recognizedCheckpoints.add(activationTime.getTime());
        int numRecognizedCheckpoints = 3;
        int recognizedCounter;


        if(recognizedCheckpoints.size() > numRecognizedCheckpoints) {
            for(int i = 0; i < originalCheckpointDeltaMS.size(); i++) {
            recognizedCounter = 0;
                for(int j = 1; j < recognizedCheckpoints.size(); j++) {
                    long recognizedDelta = Math.abs(recognizedCheckpoints.get(j) - recognizedCheckpoints.get(j-1));
                    int originalIndex = (i + j -1) % originalCheckpointDeltaMS.size();
                    if(Math.abs(recognizedDelta - originalCheckpointDeltaMS.get(originalIndex)) > 300){
                        recognizedCounter = 0;
                        break;
                    } else {
                        recognizedCounter++;
                    }

                    if(recognizedCounter == numRecognizedCheckpoints) {
                        recognizingCheckpoints = false;
                        setActiveCheckpoint(Options.VELOCITY_CHECKPOINTS, (i + numRecognizedCheckpoints - 1) % originalCheckpointDeltaMS.size());
                        recognizedCheckpoints = new ArrayList<>();
                        System.out.println("REFOUND RADAR NR: " +  (i + numRecognizedCheckpoints - 1) % originalCheckpointDeltaMS.size());
                        return;
                    }
                }
            }
        }


    }

    public static void updateCheckpointsActivation (Timestamp actualTime) {
        //TODO: adapt to general checkpoints
        long timeUntilNextCheckpoint = ((VelocityInfoBox) getNextCheckpoint().element).getTimePassedSincePrevRadar();
        timePassedSinceActiveCheckpoint = actualTime.getTime() - activeCheckpointActivationTime.getTime();

        if(!recognizingCheckpoints && ((timeUntilNextCheckpoint - timePassedSinceActiveCheckpoint) < -Options.CHECKPOINT_ACTIVATION_DELAY)) {
            recognizingCheckpoints = true;
            System.out.println("---- activate recognition ----");
        }
    }

    public static void setActiveCheckpoint (String checkpointtype, int checkpointNr) {
        activeCheckpointType = checkpointtype;
        activeCheckpointNr = checkpointNr;
        activeCheckpointActivationTime = new Timestamp(new Date().getTime());
        if(!getActiveCheckpoint().startPowerFixed) {
            getActiveCheckpoint().updateThreshold(0L, getPowerReccomendation() + Options.CHECKPOINT_ACCELLERATION_STEP);
            if(getPowerReccomendation() >= Options.DEFAULT_POWER + Options.MAX_ACCELLERATION_ROUNDS * Options.CHECKPOINT_ACCELLERATION_STEP) {
                getActiveCheckpoint().startPowerFixed = true;
            }
        } else {
            getActiveCheckpoint().increaseModificationCounter();
        }
    }


    public static void activateNextCheckpoint () {
        setActiveCheckpoint(Options.VELOCITY_CHECKPOINTS, (activeCheckpointNr+1) % checkpoints.get(activeCheckpointType).size());
        System.out.println("Checkpoint activated: " + CheckpointManager.activeCheckpointNr + ", Reccom: " + getPowerReccomendation());
    }

    public static int getPowerReccomendation() {
        return getActiveCheckpoint().getPowerRecommendation(timePassedSinceActiveCheckpoint);
    }
    public static ConfigCheckpoint getActiveCheckpoint() {
        return checkpoints.get(activeCheckpointType).get(activeCheckpointNr);
    }

    public static ConfigCheckpoint getCheckpointByNr(String type, int index) {
        return checkpoints.get(type).get(index);
    }

    public static ConfigCheckpoint getNextCheckpoint() {
        int nextCheckpointNr;
        if(!(checkpoints.get(activeCheckpointType).size() == 0)) {
             nextCheckpointNr = (activeCheckpointNr + 1) % checkpoints.get(activeCheckpointType).size();
            return checkpoints.get(activeCheckpointType).get(nextCheckpointNr);
        } else {
            System.out.println("Modulo by 0" + checkpoints.get(activeCheckpointType));
        }
        throw new Error();
    }

    public static ConfigCheckpoint getPrevCheckpoint() {
        int prevCheckpointNr = ((activeCheckpointNr == 0) ? checkpoints.get(Options.VELOCITY_CHECKPOINTS).size() : activeCheckpointNr) -1;
        return checkpoints.get(activeCheckpointType).get(prevCheckpointNr);
    }

    public static void handlePenalty() {

        if(timePassedSinceActiveCheckpoint < 1000) {
            System.out.println(getPrevCheckpoint().getPowerRecommendation(0L) == Options.DEFAULT_POWER ? "aktueller radar down" : "vorheriger radar down" );
            (getPrevCheckpoint().getPowerRecommendation(0L) == Options.DEFAULT_POWER ? getActiveCheckpoint() : getPrevCheckpoint()).handlePenalty();

        } else {
            getActiveCheckpoint().handlePenalty();
            System.out.println("aktueller radar down");

        }


    }

}
