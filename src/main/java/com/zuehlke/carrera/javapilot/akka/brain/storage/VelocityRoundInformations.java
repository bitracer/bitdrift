package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.util.ArrayList;
import java.util.Date;

public class VelocityRoundInformations extends RoundInformation{
    private ArrayList<VelocityInfoBox> velocityRadars;
    private Date modified;

    public static long lastPassedRadarTime = 0;
    public static VelocityInfoBox lastMeasurement;

    public VelocityRoundInformations(int roundNr) {
        super(roundNr);
        this.velocityRadars = new ArrayList<>();
        modified = new Date();

    }

    @Override
    public boolean isEmpty() {
        return velocityRadars.isEmpty();
    }

    @Override
    public void add(InfoBox roundInfo) {
        velocityRadars.add((VelocityInfoBox) roundInfo);
        modified.setTime(System.currentTimeMillis());
    }

    public void save() {
        InfoManager.storeRoundInfo(Options.VELOCITY_RADARS, this);
    }

    public ArrayList<VelocityInfoBox> getAll() { return velocityRadars; }

    @Override
    public Date modified() {
        return modified;
    }
}
