package com.zuehlke.carrera.javapilot.akka.brain.persistance;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

public abstract class FileWatcher extends Observable {
    private final PersistentFile file;
    private final File fileHandle;
    private long lastModified;
    private String content;
    private final static long intervall = 10;
    private final static Logger logger = LoggerFactory.getLogger(FileWatcher.class);
    public FileWatcher(PersistentFile file) throws IOException{
        this.file = file;
        content = file.readFile();
        fileHandle = new File(file.getFilename());
        lastModified = fileHandle.lastModified();
    }
    public void start(){
        Timer timer = new Timer();
        TimerTask task = poll();
        timer.scheduleAtFixedRate(task, 10, intervall*100);
    }
    public TimerTask poll(){
        return new TimerTask() {
            @Override
            public void run() {
                try {
                    if (lastModified != fileHandle.lastModified()) {
                        lastModified = fileHandle.lastModified();
                        String currentContent = file.readFile();
                        if(!content.equals(currentContent)){
                            content = currentContent;
                            setChanged();
                            notifyObservers();
                            clearChanged();
                        }
                    }
                }catch (IOException e){
                    logger.error("File could not be read: " + e.getMessage());
                }
            }
        };
    }
}
