package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.util.*;

public class ConfigCheckpoint {

    public InfoBox element;
    public Map<Long, Integer> powerTresholds;
    public boolean startPowerFixed = false;
    public Map<Long, Integer> gyroValues;
    private int previousPower;
    private long[] gyroAverage = new long[10];
    private int arrayPosition = 0;
    private int modificationCounter = 0;
    public int getModificationCounter() {
        return modificationCounter;
    }

    public ConfigCheckpoint(InfoBox infoBox) {
        this.element = infoBox;
        this.powerTresholds = new TreeMap<>();
        this.gyroValues = new TreeMap<>();
    }

    public void addPowerThreshold(long triggerTimeAfterCheckpoint, int powerlevel) {
        powerTresholds.put(triggerTimeAfterCheckpoint, powerlevel);
    }
    public void addGyroValue(long timeAfterCheckpoint, int gyro){
        if(arrayPosition == 10){
            arrayPosition = 0;
            gyroValues.put(timeAfterCheckpoint, calculateGyroAverage());
        } else {
            gyroAverage[arrayPosition++] = gyro;
        }
    }
    private int calculateGyroAverage(){
        int result = 0;
        for(long value: gyroAverage){
            result += value;
        }
        return result/10;
    }
    public void increaseModificationCounter(){
        System.out.println("Increased");
        modificationCounter++;
    }
    public void decreaseModificationCounter(){
        if(modificationCounter > 0){
            System.out.println("Decreased");
            modificationCounter--;
        }
    }
    public int adjustRecommendation(int power, long timePassedSinceActiveCheckpoint){
        if(previousPower == 0) {
            previousPower = power;
        }
        Iterator<Map.Entry<Long, Integer>> iterator = gyroValues.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Long, Integer> entry = iterator.next();
            if(Math.abs(timePassedSinceActiveCheckpoint - entry.getKey()) < 100){
                break;
            }
        }
        int gyroAverage = gyroForecast(iterator);
        if(gyroAverage != Integer.MIN_VALUE && gyroAverage < Options.GVALUE_TOLERANCE) {
            return previousPower++;
        } else {
            previousPower = power;
        }
        return power;
    }
    private int gyroForecast(Iterator<Map.Entry<Long,Integer>> iterator){
        int result = 0;
        int counter = 0;
        for(int i = 0; i < 60; i++){
            if(iterator.hasNext()) {
                counter++;
                Map.Entry<Long, Integer> entry = iterator.next();
                result += entry.getValue();
            }
        }if(counter > 40) {
            return result / counter;
        } else {
            return Integer.MIN_VALUE;
        }
    }
    public int getPowerRecommendation(long timePassedSinceActiveCheckpoint) {
        ArrayList<Long> keys = new ArrayList<>(powerTresholds.keySet());
        for(int i=keys.size()-1; i>=0 ;i--){
            if(keys.get(i) < timePassedSinceActiveCheckpoint) {
                return powerTresholds.get(keys.get(i));
            }
        }
        return Options.POWER_MEDIUM;
    }

    public void updateThreshold(long triggerTimeAfterCheckpoint, int newPowerLevel) {
        if(powerTresholds.containsKey(0L)) {
            powerTresholds.replace(triggerTimeAfterCheckpoint, newPowerLevel);
        } else {
            addPowerThreshold(triggerTimeAfterCheckpoint, newPowerLevel);
        }
    }

    public void handlePenalty() {
        if(powerTresholds == null || powerTresholds.size() == 0) return;
        startPowerFixed = true;
        updateThreshold(0L, powerTresholds.get(0L) - Options.CHECKPOINT_ACCELERATION_DOWN);
        decreaseModificationCounter();
    }
}
