package com.zuehlke.carrera.javapilot.akka.brain.InformationHolder;

public class GValueInformation {
    private int[] averageValue;
    private int currentPosition = 0;
    private int size;
    public GValueInformation(int averageSize){
        size = averageSize;
        averageValue = new int[size];
    }
    public int getAverage(){
        int count = 0;
        int sum = 0;
        for(int g: averageValue){
            if(g != 0){
                sum += g;
                count++;
            }
        }
        return sum/count;
    }
    public void pushInformation(int information){
        averageValue[currentPosition] = information;
        currentPosition = (currentPosition+1)%size;
    }
}
