package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.sql.Timestamp;

public class InfoBox {

    private long timePassedSinceStart;

    public InfoBox(long timePassedSinceStart) {
        this.timePassedSinceStart = timePassedSinceStart;
    }

    public long getTimePassedSinceStartInMS() {
        return timePassedSinceStart;
    }

    public static long timeDiffInMS(Timestamp start, Timestamp end) {
        if(start == null || end == null) return 0;
        return end.getTime() - start.getTime();
    }
}

