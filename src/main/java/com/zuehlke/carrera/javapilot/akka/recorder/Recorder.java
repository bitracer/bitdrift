package com.zuehlke.carrera.javapilot.akka.recorder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;

/**
 * Record and send instances of <code>T</code> to a remote or internal processing unit.
 * This interface is designed to be as asynchronous/non-blocking and skinny as possible.
 * The data which will be processesed by any implementing class should meet this requirement
 * as well. Since sending can be a very long process (TCP takes up to 90 seconds to abandon a
 * request), it uses <code>Runnable</code> instances to pass any information to another
 * processing instance.
 * @param <T> Class which is desired to record
 */
public interface Recorder<T> {
    /**
     * Add an instance of <code>T</code> to the Recorder collection.
     * @param t Instance of an object to save
     * @throws InterruptedException If the implementing class is using a non concurrent collection
     * it should be considered to use semaphores to prevent race conditions and concurrent modification.
     * See <code>java.util.concurrent</code> for thread-safe data structures.
     */
    void add(T t) throws InterruptedException;

    /**
     * Send every captured object to a processing instance.
     * @throws IOException When sending to a remove (non-java) instance there is a high
     * probability the implementing class could throw an <code>IOException</code>.
     */
    void send() throws IOException;

    /**
     * A <code>Runnable</code> instance to start a Thread with, which should send a single
     * instance of the recorder collection.
     * @param t An immutable object which will be sent, when executing this <code>Runnable</code>
     * @return An instance of <code>Runnable</code> which can be used by a new Thread to send
     * a single instance of the recorded type
     */
    Runnable sendObject(final T t);
}
