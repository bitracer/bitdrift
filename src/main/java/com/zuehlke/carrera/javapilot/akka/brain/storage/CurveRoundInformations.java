package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class CurveRoundInformations extends RoundInformation {

    private ArrayList<CurveInfoBox> curves;

    public enum Directions {LEFT, RIGHT, STRAIGHT}

    public static long lastPassedCurveTime = 0;
    public static CurveRoundInformations.Directions lastDirection = Directions.STRAIGHT;
    public static Timestamp lastDirectionChange = null;
    public static CurveRoundInformations activeRecognizetCurves;

    private String curveString;
    private Date modified;
    public int curveSetId;
    public int numDetection;



    public CurveRoundInformations(int roundNr) {
        super(roundNr);
        modified = new Date();
        this.curves = new ArrayList<>();
        this.numDetection = 1;
        this.curveString = "";
        this.curveSetId = 1;
    }

    @Override
    public boolean isEmpty() {
        return curves.isEmpty();
    }

    @Override
    public int hashCode() {
        return curveSetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        return curveSetId == ((CurveRoundInformations) o).curveSetId;
    }

    @Override
    public void add(InfoBox curve) {
        this.curves.add((CurveInfoBox) curve);
        modified.setTime(System.currentTimeMillis());
    }

    public String getRoundString() {
        return curveString;
    }

    private void createRoundString() {
        curves.forEach((CurveInfoBox ib) -> {
            curveString += ib.direction + "-";
            curveSetId *= 2;
            if (ib.direction.equals(Directions.RIGHT)) curveSetId++;
        });
        modified.setTime(System.currentTimeMillis());
    }

    public void save() {
        createRoundString();
        processRoundInformation();
    }

    private void processRoundInformation() {
        boolean new_flag = true;
        Iterator<CurveRoundInformations> it = (Iterator<CurveRoundInformations>) InfoManager.getAllRoundInfo(Options.CURVES).iterator();

        if (activeRecognizetCurves == null) {
            activeRecognizetCurves = this;
            return;
        }

        while (it.hasNext()) {
            CurveRoundInformations roundInfo = it.next();

            if (roundInfo.equals(this)) {
                roundInfo.numDetection += 2;

                for (int i = 0; i < curves.size(); i++)
                    roundInfo.curves.get(i).setgValue((this.curves.get(i).getGValue() + roundInfo.curves.get(i).getGValue()) / 2);

                new_flag = false;
            } else {
                roundInfo.numDetection--;
                if (roundInfo.numDetection < 0)
                    it.remove();
            }

            if (roundInfo.numDetection > activeRecognizetCurves.numDetection)
                activeRecognizetCurves = roundInfo;

        }

        if (new_flag)
            InfoManager.storeRoundInfo(Options.CURVES, this);
        modified.setTime(System.currentTimeMillis());
    }

    public static Directions getDirection(int gValue) {
        if (lastDirectionChange != null && new Timestamp(new Date().getTime()).getTime() - lastDirectionChange.getTime() < Options.TIME_TOLERANCE_BETWEEN_DIRECTIONS)
            return lastDirection;
        if (gValue <= (Options.G_SENSOR_TOLERANCE * -1)) return Directions.LEFT;
        else if (gValue >= Options.G_SENSOR_TOLERANCE) return Directions.RIGHT;
        return Directions.STRAIGHT;
    }

    public ArrayList<CurveInfoBox> getAll() { return curves; }
    @Override
    public Date modified() {
        return modified;
    }
}
