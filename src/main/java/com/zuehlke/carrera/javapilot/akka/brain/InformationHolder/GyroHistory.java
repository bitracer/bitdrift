package com.zuehlke.carrera.javapilot.akka.brain.InformationHolder;

import java.sql.Timestamp;
import java.util.*;

public class GyroHistory {
    public class GyroHistoryElement{
        private int value;
        private int power;
        public GyroHistoryElement(int gyro, int appliedPower, Timestamp occurence){
            value = gyro;
            power = appliedPower;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            GyroHistoryElement that = (GyroHistoryElement) o;

            if (value != that.value) return false;
            return power == that.power;

        }
        @Override
        public int hashCode() {
            int result = value;
            result = 31 * result + power;
            return result;
        }
    }
    private static Date raceStart = new Date();
    private static Map<Timestamp, GyroHistoryElement> history = new HashMap<>();
    public static void addGyroEvent(Timestamp timestamp, GyroHistoryElement element){
        history.put(timestamp, element);
    }

}
