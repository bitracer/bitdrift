package com.zuehlke.carrera.javapilot.akka.experimental;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.zuehlke.carrera.javapilot.akka.PowerAction;
import com.zuehlke.carrera.relayapi.messages.SensorEvent;


public class NotSoKamikazeOli extends UntypedActor {

    private AccelerationInformation accelerationInformation;
    private final ActorRef pilot;
    public NotSoKamikazeOli(ActorRef pilot){
        accelerationInformation = new AccelerationInformation(0, 20);
        this.pilot = pilot;
    }
    //Dynamische Kurven border setzten (Curve threshold)
    //Dynamische Powerlevel erkennung
    //
    @Override
    public void onReceive(Object message) throws Exception {
        SensorEvent sensorMessage = (SensorEvent)message;
        int gValue = sensorMessage.getG()[2];
        int power = powerHandler(gValue);
        pilot.tell ( new PowerAction(power), getSelf());
    }
    public static Props props ( ActorRef pilot) {
        return Props.create( NotSoKamikazeOli.class, ()->new NotSoKamikazeOli( pilot ));
    }
    private int powerHandler(int gyro){
        //TODO: get from config file
        int power = accelerationInformation.getCurrentPower();
        accelerationInformation.pushGyro(gyro);
        int increment = 5;
        int incrementOffset = accelerationInformation.getIncrementOffset();
        int gyroDriveThreshold = 20;
        if(accelerationInformation.getAverageGyro() < gyroDriveThreshold / 2){
            power = power + (increment + incrementOffset);
            accelerationInformation.setIncrementOffset(incrementOffset/2);
        } else if(accelerationInformation.getAverageGyro() < gyroDriveThreshold){
            power = power + (increment + incrementOffset);
        } else {
            power = 150;
        }
        accelerationInformation.setCurrentPower(power);
        return power;
    }
    private class AccelerationInformation {
        private int incrementOffset;
        private int currentPower;
        private int[] averageGyro;
        private int currentPosition;
        public AccelerationInformation(int currentPower, int incrementOffset){
            this.currentPower = currentPower;
            this.incrementOffset = incrementOffset;
            averageGyro = new int[10];
            currentPosition = 0;
        }
        public int getAverageGyro(){
            int count = 0;
            int sum = 0;
            for(int g: averageGyro){
                if(g != 0){
                    sum += g;
                    count++;
                }
            }
            return sum/count;
        }
        public void pushGyro(int gyro){
            averageGyro[currentPosition] = gyro;
            currentPosition = (currentPosition+1)%10;
        }
        public int getIncrementOffset() {
            return incrementOffset;
        }

        public void setIncrementOffset(int incrementOffset) {
            this.incrementOffset = incrementOffset;
        }

        public int getCurrentPower() {
            return currentPower;
        }

        public void setCurrentPower(int currentPower) {
            this.currentPower = currentPower;
        }
    }
}
