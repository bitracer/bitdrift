package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.sql.Timestamp;
import java.util.Date;
import com.zuehlke.carrera.javapilot.akka.brain.storage.CurveRoundInformations.*;
import com.zuehlke.carrera.javapilot.akka.brain.storage.CurveRoundInformations;
import com.zuehlke.carrera.javapilot.akka.recorder.Recordable;
import org.json.JSONObject;

public class CurveInfoBox extends InfoBox implements Recordable{


    private long timePassedSinceStart;
    private long timePassedSincePrevCurve;
    private int numDetection = 0;
    private String curvesDescription;
    private double gValue;
    private Date created;
    private Date modified;

    public Directions direction;

    public CurveInfoBox(int gValue, long timePassedSinceStart) {
        super(timePassedSinceStart);
        this.gValue = gValue;
        this.timePassedSinceStart = timePassedSinceStart;
        this.timePassedSincePrevCurve = CurveRoundInformations.lastPassedCurveTime == 0 ? 0 : (timePassedSinceStart - CurveRoundInformations.lastPassedCurveTime);
        CurveRoundInformations.lastPassedCurveTime = timePassedSinceStart;
        CurveRoundInformations.lastDirectionChange = new Timestamp(new Date().getTime());

        this.direction = gValue > 0 ? Directions.RIGHT : Directions.LEFT;
        created = new Date();
        modified = created;
    }

    public double getGValue() { return gValue; }
    public void setgValue(double gValue) { this.gValue = gValue; }
    public void setCurveDescription(String description) { this.curvesDescription = description; }
    public String getCurveDescription(String description) { return curvesDescription; }
    public boolean isEqualCurveDetection(String description) { return this.curvesDescription == description; }

    @Override
    public JSONObject toJson(){
        JSONObject json = new JSONObject();
        json.append("timePassedSinceStart", timePassedSinceStart);
        json.append("timePassedSincePrevCurve", timePassedSincePrevCurve);
        json.append("direction", direction);
        json.append("numDetection", numDetection);
        json.append("created", created);
        json.append("modified", modified);
        return json;
    }

    @Override
    public Date created() {
        return created;
    }

    @Override
    public Date modified() {
        return modified;
    }

    public long getTimeSinceStart () { return timePassedSinceStart; }

}
