package com.zuehlke.carrera.javapilot.akka.util;

import com.zuehlke.carrera.javapilot.akka.brain.persistance.ConfigurationFile;

import java.io.IOException;
import java.util.Scanner;

public class ConfigUtil {
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        ConfigurationFile config = ConfigurationFile.GetConfigurationFile("./options.config");
        System.out.println("Config util");
        System.out.println("====================BIT DRITS===================");
        System.out.print(">");
        try{
            config.loadConfiguration();
        } catch (IOException e){
            System.out.println("Creating new configuration file");
        }
        String input = "";
        String[] parts;
        try {
            while (!input.equals("exit")) {
                input = s.nextLine();
                parts = input.split(" ");
                switch (parts[0]) {
                    case "add":
                        config.addConfiguration(parts[1], parts[2]);
                        config.writeNewFile();
                        System.out.println("Add: " + parts[1] + config.getConfiguration(parts[1]));
                        break;
                    case "get":
                        System.out.println(config.getConfiguration(parts[1]));
                        break;
                    case "del":
                        config.deleteConfiguration(parts[1]);
                        config.writeNewFile();
                        System.out.println("Deleted: " + parts[1]);
                        break;
                    case "save":
                        config.writeNewFile();
                        System.out.println("File saved");
                        break;
                    case "load":
                        config.loadConfiguration();
                        System.out.println("File loaded");
                        break;
                    case "exit":
                        return;
                    default:
                        printHelp();
                        break;
                }
                System.out.print(">");
            }
        } catch (IOException e){
            System.err.println("Writing or reading file failed");
        } catch (Exception e){
            System.err.println("Command not found");
            printHelp();
        }
        try {
            config.writeNewFile();
        } catch (IOException e){
            System.err.println("Could not write config file");
        }
    }
    public static void printHelp(){
        System.out.println("Usage: add key value");
        System.out.println("       get key");
        System.out.println("       del key");
        System.out.println("Changing a value can be archived by adding a new value with the same key");
    }
}