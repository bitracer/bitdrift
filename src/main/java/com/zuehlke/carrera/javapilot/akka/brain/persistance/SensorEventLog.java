package com.zuehlke.carrera.javapilot.akka.brain.persistance;

import org.joda.time.DateTime;

import java.io.IOException;

public class SensorEventLog extends EventLog {

    public SensorEventLog(String filename) throws IOException {
        super(filename);
    }
    public SensorEventLog() throws IOException {
        super("sensor-event-log_" + DateTime.now().toString("MM.dd-HH:mm:ss") + ".log");
    }
}
