package com.zuehlke.carrera.javapilot.akka.brain.storage;

import sun.awt.geom.Curve;

import java.util.*;

public class InfoManager {

    private static Map<String, ArrayList<RoundInformation>> infoStorage = new HashMap<>();
    private static ArrayList<RoundSectionInformation> roundSections = new ArrayList<>();
    private static int actualRoundNr = 1;

    public static void register(String type) {
        infoStorage.put(type, new ArrayList<>());
    }

    public static RoundInformation getRoundInfo (String type, int roundnr) {
        if(roundnr <= 0 || roundnr > infoStorage.get(type).size()) return null;
        return infoStorage.get(type).get(roundnr-1);
    }

    public static void increaseRoundCounter () {
        actualRoundNr++;
    }

    public static int getRoundNr () {
        return actualRoundNr;
    }

    public static ArrayList<? extends RoundInformation> getAllRoundInfo(String type) {
        return infoStorage.get(type);
    }

    public static <T extends RoundInformation> void storeRoundInfo(String type, T RoundInfo) {
        if(RoundInfo.isEmpty()) return;
        infoStorage.get(type).add(RoundInfo);
    }

    /*
    public static void createRoundSections() {
        ArrayList<CurveInfoBox> curves = CurveRoundInformations.activeRecognizetCurves.getAll();
        ArrayList<VelocityInfoBox> velocityRadars = ((VelocityRoundInformations)InfoManager.getRoundInfo(Options.VELOCITY_RADARS, CurveRoundInformations.activeRecognizetCurves.getRoundNr())).getAll();
        if(velocityRadars.size() <= 1) return;
        VelocityInfoBox prevRadar = velocityRadars.get( 0 );
        VelocityInfoBox nextRadar;
        CurveInfoBox curve;
        RoundSectionInformation roundSection;
        Iterator<CurveInfoBox> it = curves.iterator();

        // TODO: Code can be a more beautiful
        for(int i = 1; i < velocityRadars.size(); i++) {
            nextRadar = velocityRadars.get(i);
            roundSection = new RoundSectionInformation(new VelocityConfigCheckpoint(prevRadar), new VelocityConfigCheckpoint(nextRadar), i);
            while(it.hasNext()) {
                curve = it.next();
                if(curve.getTimeSinceStart() > prevRadar.getTimePassedSinceStartInMS() && curve.getTimeSinceStart() < nextRadar.getTimePassedSinceStartInMS())
                {
                    roundSection.addConfigCheckpoint(new CurveConfigCheckpoint(curve));
                    it.remove();
                }
            }
            roundSections.add(roundSection);
            it = curves.iterator();
        }

        it = curves.iterator();
        roundSection = new RoundSectionInformation(new VelocityConfigCheckpoint(velocityRadars.get(velocityRadars.size()-1)),new VelocityConfigCheckpoint(velocityRadars.get(0)), velocityRadars.size());
        while(it.hasNext()) {
            curve = it.next();
            roundSection.addConfigCheckpoint( new CurveConfigCheckpoint(curve) );
        }
        roundSections.add(roundSection);

        //DEBUG PRINT
        String print = "", curveString= "", gValueString = "";
        for(RoundSectionInformation roundsection : roundSections) {
            print += "\n== Section: " + roundsection.sectionNr + " ==\n";
            ListIterator cpIt = roundsection.getSectionCheckpoints().listIterator();
            curveString = "Curves Directions: ";
            gValueString = "Curves gValues: ";
            while(cpIt.hasNext()) {
                CurveConfigCheckpoint cp = (CurveConfigCheckpoint) cpIt.next();
                curveString += cp.element.direction + "--";
                gValueString += cp.element.getGValue() + "--";
            }
            print += curveString + '\n' + gValueString + '\n';
        }
        System.out.println(print);


    }*/

}
