package com.zuehlke.carrera.javapilot.akka.library;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadPool {
    private BlockingQueue<Runnable> taskQueue;
    private List<PoolThread> threads = new ArrayList<>();
    private boolean isStopped;

    public ThreadPool(int noOfThreads, int maxNoOfTask){
        isStopped = false;
        taskQueue = new LinkedBlockingQueue<>(maxNoOfTask);
        for(int i = 0; i<noOfThreads; i++){
            threads.add(new PoolThread(taskQueue));
        }
        threads.forEach((PoolThread t) -> t.start());
    }
    public synchronized void execute(Runnable task) {
        if(this.isStopped){
            throw new IllegalStateException("Thread pool is stopped");
        }
        taskQueue.add(task);
    }
    public synchronized void stop(){
        isStopped = true;
        threads.forEach((PoolThread t) -> t.interrupt());
    }

}
