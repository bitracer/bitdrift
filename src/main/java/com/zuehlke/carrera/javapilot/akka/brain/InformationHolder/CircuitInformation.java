package com.zuehlke.carrera.javapilot.akka.brain.InformationHolder;

import com.zuehlke.carrera.javapilot.akka.brain.persistance.ConfigurationFile;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import java.util.Iterator;
import java.util.LinkedList;

public class CircuitInformation{
    private final static int averageSize = 10;
    private Round currentRound;
    private Round previousRound;
    private LinkedList<Round> rounds;
    private Iterator<GyroEvent> previousRoundIterator;
    public CircuitInformation() {
        currentRound = new Round(1, new LinkedList<GyroEvent>());
        rounds = new LinkedList<>();
        rounds.add(currentRound);
    }
    public void nextRound(){
        previousRound = currentRound;
        currentRound = new Round(currentRound.round+1, new LinkedList<GyroEvent>());
        rounds.add(currentRound);
        previousRoundIterator = previousRound.events.iterator();
    }
    public int getPreviousRoundEvent(DateTime now){
        GyroEvent previousRoundEvent;
        while(previousRoundIterator.hasNext()){
            previousRoundEvent = previousRoundIterator.next();
            int previousRoundDiff = getTimeDifference(previousRoundEvent.occurence, previousRound.roundStart);
            int currentRoundDiff = getTimeDifference(now, currentRound.roundStart);
            if(currentRoundDiff - previousRoundDiff > 0){
                return previousRoundEvent.gvalue;
            }
        }
        return Integer.MAX_VALUE;
    }
    public boolean push(int gyro, DateTime now){
        return currentRound.events.add(new GyroEvent(now, gyro));
    }

    private static int getTimeDifference(DateTime now, DateTime then){
        return Seconds.secondsBetween(now, then).getSeconds();
    }
    private class Round {
        private DateTime roundStart;
        private int round;
        private LinkedList<GyroEvent> events;
        public Round(int round, LinkedList<GyroEvent> events){
            this.events = events;
            this.round = round;
            roundStart = new DateTime();
        }
    }
    private class GyroEvent {
        private DateTime occurence;
        private int gvalue;
        public GyroEvent(DateTime now, int value){
            this.occurence = now;
            this.gvalue = value;
        }
    }
}
