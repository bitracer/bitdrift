package com.zuehlke.carrera.javapilot.akka.brain.storage;

public class CurveConfigCheckpoint extends ConfigCheckpoint {

    CurveInfoBox element;

    public CurveConfigCheckpoint(CurveInfoBox infoBox) {
        super(infoBox);
        this.element = infoBox;
    }
}
