package com.zuehlke.carrera.javapilot.akka.recorder;

import com.zuehlke.carrera.javapilot.akka.brain.storage.CurveInfoBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * A CurveInfoBox recorder to store and send curve information to a HTTP client
 */
public class CurveInfoBoxRecorder  extends InfoBoxRecorder<CurveInfoBox>{
    private final static String path =  "api/curve/curve";

    /**
     * Create a CurveInfoBoxRecorder instance.
     * @param address HTTP client URI e.g. "http://localhost"
     */
    public CurveInfoBoxRecorder(String address){
        super(address, path);
    }
}
