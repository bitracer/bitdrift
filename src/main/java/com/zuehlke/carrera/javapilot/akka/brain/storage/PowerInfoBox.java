package com.zuehlke.carrera.javapilot.akka.brain.storage;

import com.zuehlke.carrera.javapilot.akka.recorder.Recordable;
import org.json.JSONObject;

import java.util.Date;

public class PowerInfoBox implements Recordable{
    private Date created;
    private int powerLevel;

    public PowerInfoBox(int powerLevel){
        this.powerLevel = powerLevel;
        created = new Date();
    }
    @Override
    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.append("powerLevel", powerLevel);
        json.append("created", created);
        return json;
    }

    @Override
    public Date created() {
        return created;
    }

    @Override
    public Date modified() {
        return created;
    }
}
