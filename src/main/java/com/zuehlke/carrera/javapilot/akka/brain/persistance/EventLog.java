package com.zuehlke.carrera.javapilot.akka.brain.persistance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public abstract class EventLog extends PersistentFile{
    private FileWriter fileWriter;
    private BlockingQueue<String> queue;
    private TimerTask task;
    private final static long intervall = 10;
    private final static int capacity = 1024;
    private final static Logger logger = LoggerFactory.getLogger(EventLog.class);
    public EventLog(String filename) throws IOException{
        super(filename);
        fileWriter = new FileWriter(filename);
        queue = new ArrayBlockingQueue<>(capacity);
        intervallSave(intervall);
    }
    public void write(String content) throws IOException {
        fileWriter.append(content);
    }
    public void append(String content){
        if(queue.size() < capacity) {
            queue.add(content);
        } else {
            logger.error("Write queue is full. Cannot append to file");
        }
    }
    private void intervallSave(long intverall){
        Timer timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                int length = queue.size();
                try {
                    for (int i = 0; i < length; i++) {
                        try {
                            appendToFile(queue.take() + ";");
                        } catch (InterruptedException e) {
                            logger.error("Thread was interrupted: " + e.getMessage());
                        }
                    }
                } catch (IOException e) {
                    logger.error("File operation failed: " + e.getMessage());
                }
                logger.debug("EventLog written");
            }
        };
        timer.scheduleAtFixedRate(task, 10, intverall*100);
    }
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        task.cancel();
        queue.forEach(s -> append(s + ";"));
        fileWriter.flush();
        fileWriter.close();
    }
}
