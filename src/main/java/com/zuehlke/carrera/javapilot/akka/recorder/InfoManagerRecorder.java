package com.zuehlke.carrera.javapilot.akka.recorder;

import com.zuehlke.carrera.javapilot.akka.library.HTTPRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public abstract class InfoManagerRecorder<T extends Recordable> implements Recorder<T> {
    private final String address;
    private final String path;
    private static Logger logger = LoggerFactory.getLogger(InfoManagerRecorder.class);

    public InfoManagerRecorder(final String address, final String path){
        this.address = address;
        this.path = path;
    }

    public String getAddress() {
        return address;
    }
    @Override
    public Runnable sendObject(final T object){
        return new Runnable() {
            @Override
            public void run() {
                try {
                    HTTPRequest request= new HTTPRequest(address + path, HTTPRequest.Method.POST);
                    request.send(object.toJson().toString(), HTTPRequest.parseMediaType("application/json"));
                } catch (IOException e){
                    logger.warn("Sending information unsuccessful: " + e.getMessage());
                }
            }
        };
    }

}
