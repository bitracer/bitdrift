package com.zuehlke.carrera.javapilot.akka.experimental;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.zuehlke.carrera.javapilot.akka.PowerAction;
import com.zuehlke.carrera.javapilot.akka.brain.InformationHolder.AccelerationInformation;
import com.zuehlke.carrera.javapilot.akka.brain.persistance.ConfigurationFile;
import com.zuehlke.carrera.javapilot.akka.brain.persistance.ConfigurationFileWatcher;
import com.zuehlke.carrera.javapilot.akka.brain.storage.*;
import com.zuehlke.carrera.relayapi.messages.PenaltyMessage;
import com.zuehlke.carrera.relayapi.messages.SensorEvent;
import com.zuehlke.carrera.relayapi.messages.VelocityMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.sql.Timestamp;

import com.zuehlke.carrera.javapilot.akka.brain.storage.CurveRoundInformations.*;

public class KamikazeOli extends UntypedActor {
    public static final String address = "http://127.0.0.1:3000/";
    private final ActorRef pilot;
    private Timestamp initRound;
    private Timestamp initRace;
    private int power;
    public Timestamp slowDownTimeInterval;
    public ArrayList<VelocityInfoBox> learndVelocityRadars;
    private boolean isStarting = true;
    private AccelerationInformation accellerationInformation;
    private ConfigurationFile config;
    private ConfigurationFileWatcher watcher;

    private boolean debugOut = false;
    private int outCount = 0;


    private VelocityRoundInformations actualVelocityRoundInfo;
    private CurveRoundInformations actualCurveRoundInfo;

    Timestamp actualTime;
    long timeSinceRoundStartInMS;
    long timeSinceRaceStartInMS;
    private final static Logger logger = LoggerFactory.getLogger(KamikazeOli.class);

    public KamikazeOli( ActorRef pilot ) {
        this.pilot = pilot;
        this.initRound = new Timestamp(0);
        config = ConfigurationFile.GetConfigurationFile("options.config");
        try {
            config.loadConfiguration();
        } catch (IOException e){
            logger.warn("Error while loading configuration file. Using new one. "  + e.getMessage());
        }
        try {
            //sensorEventLog = new SensorEventLog();
            watcher = new ConfigurationFileWatcher(config);
            watcher.start();
        } catch(IOException e){
            logger.error("Instanciation of SensorEventLog failed: " + e.getMessage());
        }

        this.power = Integer.parseInt(config.getConfiguration("DEFAULT_POWER"));
        this.slowDownTimeInterval = new Timestamp(0);
        this.learndVelocityRadars = new ArrayList<>();

        InfoManager.register(config.getConfiguration("VELOCITY_RADARS"));
        InfoManager.register(config.getConfiguration("CURVES"));
        CheckpointManager.register(config.getConfiguration("VELOCITY_CHECKPOINTS"));

        actualCurveRoundInfo = new CurveRoundInformations(1);
        actualVelocityRoundInfo = new VelocityRoundInformations(1);
        accellerationInformation = new AccelerationInformation(0, 20);
    }

    public static Props props ( ActorRef pilot) {
        return Props.create( KamikazeOli.class, ()->new KamikazeOli( pilot ));
    }

    @Override
    public void onReceive(Object msg) throws Exception {
        // Get Eventtype from message as String (just preparation for the switch statement)
        String ClassName = msg.getClass().getName();
        ClassName = ClassName.substring(ClassName.lastIndexOf(".")+1);

        actualTime = new Timestamp(new Date().getTime());
        timeSinceRoundStartInMS = InfoBox.timeDiffInMS(initRound, actualTime);
        timeSinceRaceStartInMS = InfoBox.timeDiffInMS(initRace, actualTime);

        if(Options.isInLearnStage) {
            switch(ClassName) {
                case "SensorEvent": LearnStageSensorEvent((SensorEvent) msg); break;
                case "VelocityMessage": LearnStageVelocityMessage((VelocityMessage) msg); break;
                case "PenaltyMessage": PenaltyMessage((PenaltyMessage) msg);break;

                default: unhandled(msg);
            }
        } else {
            CheckpointManager.updateCheckpointsActivation(actualTime);
            switch(ClassName) {
                case "SensorEvent": SensorEvent((SensorEvent) msg); break;
                case "VelocityMessage": VelocityMessage((VelocityMessage) msg); break;
                case "PenaltyMessage": PenaltyMessage((PenaltyMessage) msg);break;

                default: unhandled(msg);
            }
        }
    }
    private int accelerator(int gyro){
        int power = accellerationInformation.getCurrentPower();
        accellerationInformation.pushInformation(gyro);
        int increment = Integer.parseInt(config.getConfiguration("ACCELERATION_INVERVAL"));
        int incrementOffset = accellerationInformation.getIncrementOffset();
        int gyroDriveThreshold = Integer.parseInt(config.getConfiguration("ACCELERATION_GYRO_THRESHOLD"));
        if(accellerationInformation.getAverage() < gyroDriveThreshold / 2){
            power = power + (increment + incrementOffset);
            accellerationInformation.setIncrementOffset(incrementOffset/2);
        } else if(accellerationInformation.getAverage() < gyroDriveThreshold){
            power = power + (increment + incrementOffset);
        } else {
            isStarting = false;
            logger.info("Last power was: "+ power);
            config.addConfiguration("POWER_MEDIUM", Integer.toString(power));
            Options.POWER_MEDIUM = 140;
            //Options.POWER_MEDIUM = power;
            config.addConfiguration("POWER_LOW", Integer.toString(power/2));
            Options.POWER_LOW = power/2;
            logger.info("Low power set to: " + Options.POWER_LOW);
            config.addConfiguration("POWER_HIGH", Integer.toString(power*2));
            Options.POWER_HIGH = power*2;
            logger.info("High power set to: " + Options.POWER_HIGH);
            Options.MAX_GYRO_VALUE = power*20;
            logger.info("Max Gyro Value set to: " + Options.MAX_GYRO_VALUE);
        }
        accellerationInformation.setCurrentPower(power);

        return power;
    }

    /**
     * The Sensor Event is fired often and provides a lot of informations.
     * Anyway the only usable Info is the G-Sensivity
     * @param msg
     */
    private void LearnStageSensorEvent(SensorEvent msg) {
        int gValue = msg.getG()[2];
        int power = 0;
        if (isStarting) {
            power = accelerator(gValue);
        } else {
            if (initRace == null) initRace = new Timestamp(new Date().getTime());

            Directions actualDirection = CurveRoundInformations.getDirection(gValue);

            // Add curve Information
            if (actualDirection != CurveRoundInformations.lastDirection && !actualDirection.equals(CurveRoundInformations.Directions.STRAIGHT)) {
                actualCurveRoundInfo.add(new CurveInfoBox(gValue, timeSinceRoundStartInMS));
            }

            // Try to regulate the speed at first rounds until all curves are detected
            if (CurveRoundInformations.lastDirectionChange != null
                    && CurveRoundInformations.lastDirection.equals(Directions.STRAIGHT)
                    && InfoBox.timeDiffInMS(CurveRoundInformations.lastDirectionChange, actualTime) > Options.MAX_ACCELERATION_TIME
                    && (!Options.isInLearnStage || (InfoBox.timeDiffInMS(slowDownTimeInterval, actualTime) > Options.MAX_ACCELERATION_TIME))) {

                if (Options.isInLearnStage && !isStarting) {
                    power = Options.DEFAULT_SLOW_DOWN_POWER;
                    slowDownTimeInterval = actualTime;
                }
            }
            // Reset curve informations and tell pilot the calculated power. Reset power to default.
            CurveRoundInformations.lastDirection = actualDirection;
            power = Options.POWER_MEDIUM;
        }
        pilot.tell(new PowerAction(power), getSelf());

    }

    /**
     * The VelocityMessage is fired when we pass a Velocity-Radar (red on the simulator)
     * @param msg
     */
    private void VelocityMessage(VelocityMessage msg) {
        //actualVelocityRoundInfo.add(new VelocityInfoBox(timeSinceRoundStartInMS, msg.getVelocity(), actualTime));

        /* AUSFALL

        if(!CheckpointManager.recognizingCheckpoints && outCount < 3) {
            boolean rand = Math.random() > 0.3;
            debugOut = rand;
            System.out.println("rand: " + rand);
            if(debugOut) {
                System.out.println("--- Ausfall ---");
                outCount++;
                return;
            }
        }

        */

        if(CheckpointManager.recognizingCheckpoints) {
            CheckpointManager.updateCheckpointRecognition(actualTime);
        } else {
            CheckpointManager.activateNextCheckpoint();
        }
    }
    private void LearnStageVelocityMessage(VelocityMessage msg) {
        learndVelocityRadars.add(new VelocityInfoBox(timeSinceRaceStartInMS, msg.getVelocity(), actualTime));
        int numVelocityRadars = learndVelocityRadars.size();


        System.out.println("added: " + learndVelocityRadars.get(numVelocityRadars-1).getTimePassedSinceStartInMS());
        System.out.println("num radars: " + numVelocityRadars);

        //TODO: Ausfall von Sensor abfangen.
        if(numVelocityRadars % 2 != 0 && numVelocityRadars > 4) {
            int midIndex = numVelocityRadars / 2 - 1;

            VelocityInfoBox lastRadar = learndVelocityRadars.get(numVelocityRadars -1);
            VelocityInfoBox penUltimateRadar = learndVelocityRadars.get(numVelocityRadars -2);

            boolean foundRadarCycle = true;
            ArrayList<Long> timePassedSincePrevRadar = new ArrayList<>();

            for(int i = 0; i <= midIndex && foundRadarCycle; i++) {

                long timePassedSincePrevRadarFirstRound = (i == 0) ? (lastRadar.getTimePassedSinceStartInMS() - penUltimateRadar.getTimePassedSinceStartInMS())
                        : learndVelocityRadars.get(i).getTimePassedSinceStartInMS() - learndVelocityRadars.get(i -1).getTimePassedSinceStartInMS();
                long timePassedSincePrevRadarSecondRound = learndVelocityRadars.get(i + midIndex + 1).getTimePassedSinceStartInMS() - learndVelocityRadars.get(i + midIndex).getTimePassedSinceStartInMS();

                long deltaMS = Math.abs(timePassedSincePrevRadarFirstRound - timePassedSincePrevRadarSecondRound);

                System.out.println("Delta: " + deltaMS);

                if(deltaMS > Options.TIME_SINCE_PREV_RADAR_TOLERANCE) {
                    foundRadarCycle = false;
                    break;
                }
                timePassedSincePrevRadar.add(i, timePassedSincePrevRadarFirstRound);

            }

            if(foundRadarCycle) {
                for(int i = 0; i <= midIndex; i++) {
                    learndVelocityRadars.get(i).saveTimePassedSincePrevRadar(timePassedSincePrevRadar.get(i));
                    CheckpointManager.add(Options.VELOCITY_CHECKPOINTS, new VelocityConfigCheckpoint(learndVelocityRadars.get(i)));
                    CheckpointManager.getCheckpointByNr(Options.VELOCITY_CHECKPOINTS,i).addPowerThreshold(0L, Options.POWER_MEDIUM);
                }
                CheckpointManager.saveOriginalCheckpointDeltaData(timePassedSincePrevRadar);
                CheckpointManager.setActiveCheckpoint(Options.VELOCITY_CHECKPOINTS, 0);

                // -- BEGIN TEST DATA -- //
                //TreeMap<Long, Integer> testPowerTresholds = new TreeMap<>();
                //testPowerTresholds.put(4000L, 80);
                //testPowerTresholds.put(1000L, 120);
                //testPowerTresholds.put(8000L, 100);
                //testPowerTresholds.put(0L, 30);
                //CheckpointManager.getActiveCheckpoint().powerTresholds = testPowerTresholds;
                // -- END TEST DATA -- //

                System.out.println("finish learning");
                Options.isInLearnStage = false;
            }
        }

    }

    private void SensorEvent(SensorEvent msg) {
        int power = CheckpointManager.getPowerReccomendation();
        if(CheckpointManager.checkIfStable()) {
            int adjustment = CheckpointManager.getActiveCheckpoint().adjustRecommendation(power, CheckpointManager.getTimePassedSinceActiveCheckpoint());
            //logger.info("Adjusted power would be: " + (adjustment));
            power = adjustment;
        }
        int gValue = msg.getG()[2];
        if(emergencyStop(gValue)) power = Options.DEFAULT_POWER;
        CheckpointManager.getActiveCheckpoint().addGyroValue(CheckpointManager.getTimePassedSinceActiveCheckpoint(), gValue);


        if(CheckpointManager.recognizingCheckpoints) power = Options.POWER_MEDIUM;

        System.out.println("power here");

        pilot.tell ( new PowerAction(power), getSelf());
    }

    private void PenaltyMessage(PenaltyMessage msg) {
        System.out.println("---PENALTY---");
        CheckpointManager.handlePenalty();
    }
    private boolean emergencyStop(int gyro){
        return gyro > Options.MAX_GYRO_VALUE;
    }
}
