package com.zuehlke.carrera.javapilot.akka.recorder;

import com.zuehlke.carrera.javapilot.akka.library.HTTPRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * A InfoBox recorder and store class. It is used to record and/or send the recorded item to a
 * HTTP server instance.
 * @param <T> Type of the recorder objects
 */
public abstract class InfoBoxRecorder<T extends Recordable> implements Recorder<T> {
    private final String address;
    private final String path;
    private List<T> infoBoxList;
    private Semaphore semaphore;
    private static Logger logger = LoggerFactory.getLogger(InfoBoxRecorder.class);

    /**
     * Create a InfoBoxRecorder instance
     * @param address HTTP server address to send every recorded instance to
     * @param path Relative path to dispatch the request (e.g: 'api/data/resource/push')
     */
    public InfoBoxRecorder(String address, String path){
        infoBoxList = new LinkedList<>();
        semaphore = new Semaphore(1);
        this.address = address;
        this.path = path;
    }
    @Override
    public void add(T infoBox){
        new Thread(() -> {
            try{
                while(!semaphore.tryAcquire()){
                    Thread.sleep(10);
                }
                semaphore.acquire();
            } catch (InterruptedException e){
                logger.warn("Interrupted while adding element: " + e.getMessage());
            }
            infoBoxList.add(infoBox);
            semaphore.release();
        }).start();
    }
    @Override
    public void send(){
        new Thread(() -> {
            List<T> temp;
            try{
                temp = cloneAndReset();
                temp.forEach((T ib) -> new Thread(sendObject(ib)).start());
            } catch (InterruptedException e){
                logger.warn("Interrupted while sending object: " + e.getMessage());
            }
        }).start();
    }
    @Override
    public Runnable sendObject(final T object){
        return new Runnable(){
            @Override
            public void run() {
                try {
                    HTTPRequest request= new HTTPRequest(address + path, HTTPRequest.Method.POST);
                    request.send(object.toJson().toString(), HTTPRequest.parseMediaType("application/json"));
                    logger.debug("InfoBox sent. Answer: " + request.getResponse());
                } catch (IOException e){
                    logger.warn("Sending object unsuccessful: " + e.getMessage());
                }
            }
        };
    }
    protected List<T> cloneAndReset() throws InterruptedException{
        List<T> newList = new LinkedList<>();
        while(!semaphore.tryAcquire()){
            Thread.sleep(5);
        }
        semaphore.acquire();
        infoBoxList.forEach((T ib) -> newList.add(ib));
        infoBoxList = new LinkedList<>();
        semaphore.release();
        return newList;
    }
    public void sendSingleObject(T object){
        new Thread(sendObject(object)).start();
    }
    public String getAddress() {
        return address;
    }
}
