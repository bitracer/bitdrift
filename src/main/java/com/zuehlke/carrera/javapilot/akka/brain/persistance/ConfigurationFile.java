package com.zuehlke.carrera.javapilot.akka.brain.persistance;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.Observable;
import java.util.Observer;

public class ConfigurationFile extends PersistentFile implements Observer {
    private JSONObject json;
    private final static Logger logger = LoggerFactory.getLogger(ConfigurationFile.class);
    private static ConfigurationFile configurationFile;

    private ConfigurationFile(String filename) {
        super(filename);
        json = new JSONObject();
    }

    public static ConfigurationFile GetConfigurationFile(String filename) {
        if(configurationFile == null){
            configurationFile = new ConfigurationFile(filename);
        }
        return configurationFile;
    }

    public void loadConfiguration() throws IOException {
        String content = super.readFile();
        json = new JSONObject(content);
    }

    public void writeNewFile() throws IOException {
        super.writeNewFile(json.toString(4));
    }

    public void setConfiguration(String key, String newValue) {
        if (json.has(key)) {
            json.remove(key);
            json.put(key, newValue);
        } else {
            throw new InvalidParameterException("Key: " + key + " not found");
        }
    }

    public void deleteConfiguration(String key) {
        if (json.has(key)) {
            json.remove(key);
        } else {
            throw new InvalidParameterException("Key: " + key + " not found");
        }
    }

    public void addConfiguration(String key, String value) {
        json.put(key, value);
    }

    public String getConfiguration(String key) {
        return json.getString(key);
    }

    @Override
    public void update(Observable o, Object ar) {
        try {
            loadConfiguration();
            logger.debug("File changed. Reload");
        } catch (IOException e) {
            logger.error("File could not be read: " + e.getMessage());
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        writeNewFile();
    }
}
