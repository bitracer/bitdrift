package com.zuehlke.carrera.javapilot.akka.brain.storage;

import com.zuehlke.carrera.javapilot.akka.recorder.Recordable;
import org.json.JSONObject;

import java.util.Date;

public class GyroInfoBox implements Recordable{
    private Date created;
    private long gyroValue;

    public GyroInfoBox(long gyroValue){
        this.gyroValue = gyroValue;
        created = new Date();
    }
    @Override
    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.append("gyroValue", gyroValue);
        json.append("created", created);
        return json;
    }

    @Override
    public Date created() {
        return created;
    }

    @Override
    public Date modified() {
        return created;
    }
}
