package com.zuehlke.carrera.javapilot.akka.brain.persistance;

import java.io.*;

public abstract class PersistentFile {
    private File file;
    public PersistentFile(String filename) {
        file = new File(filename);
    }

    protected String readFile() throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        StringBuilder content = new StringBuilder();
        String currentLine;
        while((currentLine = bufferedReader.readLine()) != null){
            content.append(currentLine);
        }
        bufferedReader.close();
        return content.toString();
    }
    private void writeFile(String content, boolean append) throws IOException{
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, append));
        bufferedWriter.write(content);
        bufferedWriter.flush();
        bufferedWriter.close();
    }
    protected void appendToFile(String content) throws IOException{
        writeFile(content, true);
    }
    protected void writeNewFile(String content) throws IOException{
        writeFile(content, false);
    }
    public String getFilename() {
        return file.getName();
    }
}
