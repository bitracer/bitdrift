package com.zuehlke.carrera.javapilot.akka.recorder;

import com.zuehlke.carrera.javapilot.akka.library.HTTPRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public abstract class MessageRecorder<T extends Recordable> implements Recorder<T>{
    private List<T> messageList;
    private Semaphore semaphore;
    private String address;
    private String path;
    private static Logger logger = LoggerFactory.getLogger(MessageRecorder.class);
    public MessageRecorder(String address, String path) {
        this.address = address;
        this.path = path;
        messageList = new LinkedList<>();
        semaphore = new Semaphore(1);
    }

    @Override
    public void add(T t) throws InterruptedException {
        new Thread(() -> {
                try {
                    while(!semaphore.tryAcquire()) {
                        Thread.sleep(10);
                    }
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    logger.warn("Failed to save message: " + e.getMessage());
                }
            messageList.add(t);
            semaphore.release();
        }).start();
    }
    protected List<T> getMessageList(){
        return messageList;
    }
    protected List<T> cloneAndReset() throws InterruptedException {
        List<T> newCollection = new LinkedList<>();
        while(!semaphore.tryAcquire()){
            Thread.sleep(10);
        }
        semaphore.acquire();
        messageList.forEach((T message) -> newCollection.add(message));
        messageList = new LinkedList<>();
        semaphore.release();
        return newCollection;
    }
    @Override
    public Runnable sendObject(T object) {
        return new Runnable() {
            @Override
            public void run() {
                try{
                    HTTPRequest request = new HTTPRequest(address + path, HTTPRequest.Method.POST);
                    request.send(object.toJson().toString(), HTTPRequest.parseMediaType("application/json"));
                } catch (IOException e){
                    logger.warn("Failed to send message: " + e.getMessage());
                }
            }
        };
    }
}
