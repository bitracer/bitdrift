package com.zuehlke.carrera.javapilot.akka.recorder;

import com.zuehlke.carrera.javapilot.akka.brain.storage.GyroInfoBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class GyroMessageRecorder extends MessageRecorder<GyroInfoBox> {
    private static final String path = "api/gyro/set";
    private static Logger logger = LoggerFactory.getLogger(GyroMessageRecorder.class);
    public GyroMessageRecorder(String address) {
        super(address, path);
    }

    @Override
    public void send() throws IOException {
        new Thread(() -> {
            List<GyroInfoBox> temp;
            try{
                temp = cloneAndReset();
                temp.forEach((GyroInfoBox g) -> new Thread(sendObject(g)).start());
            } catch (InterruptedException e) {
                logger.warn("Failed to send list: " + e.getMessage());
            }
            temp = null;
        }).start();
    }
    public void sendSingleObject(GyroInfoBox infoBox){
        new Thread(sendObject(infoBox)).start();
    }
}
