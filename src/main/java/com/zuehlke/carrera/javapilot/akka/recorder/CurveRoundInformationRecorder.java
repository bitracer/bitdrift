package com.zuehlke.carrera.javapilot.akka.recorder;

import com.zuehlke.carrera.javapilot.akka.brain.storage.CurveRoundInformations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class CurveRoundInformationRecorder extends InfoManagerRecorder<CurveRoundInformations>{
    private List<CurveRoundInformations> list;
    private Semaphore semaphore;
    private static final String path = ""; //TODO: set path
    private static Logger logger = LoggerFactory.getLogger(CurveRoundInformationRecorder.class);

    public CurveRoundInformationRecorder(String address){
        super(address,path);
        semaphore = new Semaphore(1);
        list = new LinkedList<>();
    }
    @Override
    public void add(CurveRoundInformations curveRoundInformations) throws InterruptedException {
        new Thread(() -> {
            try {
                while (!semaphore.tryAcquire()) {
                    Thread.sleep(10);
                }
                semaphore.acquire();
                list.add(curveRoundInformations);
                semaphore.release();
            } catch (InterruptedException e){
                logger.warn("Error adding curve to collection: " + e.getMessage());
            }
        }).start();
    }
    @Override
    public void send() {
        new Thread(() -> {
            List<CurveRoundInformations> temp;
            try {
                while (!semaphore.tryAcquire()){
                    Thread.sleep(10);
                }
                semaphore.acquire();
                temp = cloneAndResetList();
                semaphore.release();
                temp.forEach((CurveRoundInformations c) -> new Thread(sendObject(c)).start());
            } catch (InterruptedException e){
                logger.warn("Interruptet while cloning list: " + e.getMessage());
            }
        }).start();
    }

    @Override
    public Runnable sendObject(CurveRoundInformations curveRoundInformations) {
        return null;
    }

    private List<CurveRoundInformations> cloneAndResetList() throws InterruptedException {
        List<CurveRoundInformations> newCollection = new LinkedList<>();
        while(!semaphore.tryAcquire()){
            Thread.sleep(10);
        }
        semaphore.acquire();
        list.forEach((CurveRoundInformations curveInfo) -> newCollection.add(curveInfo));
        list = new LinkedList<>();
        semaphore.release();
        return newCollection;

    }
    public void sendSingleObject(CurveRoundInformations curveRoundInformations) {
        //TODO implement
    }

}
