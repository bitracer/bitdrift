package com.zuehlke.carrera.javapilot.akka.recorder;

import org.json.JSONObject;

import java.util.Date;

/**
 * The Recordable interface will be implemented when a Recorder class wants to use the implementing class to record as generic datatype.
 * Since JSON is a multipurpose format, it uses a <code>org.json.JSONObject</code> to pass information to another instance. This can be
 * omitted when sending it to another Java instance, but it is highly recommended not to serialize it an de serialize it at the other end.
 */
public interface Recordable {
    /**
     * The implementing class should write all important fields and parameters into this JSON instance in order to dispatch the JSON
     * string.
     * @return JSONObject with important values
     */
    JSONObject toJson();

    /**
     * When recording things, it is very useful to know, when this object was created. This Date object should only be
     * instanciated and never changed.
     * @return
     */
    Date created();

    /**
     * Tracking the diverse states of an instance will need an information about whether it has been modified or not
     * since the last event. This timestamp should be re-set every time anything in this instance is changed.
     * @return
     */
    Date modified();
}
