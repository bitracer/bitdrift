package com.zuehlke.carrera.javapilot.akka.brain.persistance;

import java.io.IOException;

public class ConfigurationFileWatcher extends FileWatcher{
    public ConfigurationFileWatcher(ConfigurationFile configurationFile) throws IOException{
        super(configurationFile);
        addObserver(configurationFile);
    }
}