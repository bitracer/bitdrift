package com.zuehlke.carrera.javapilot.akka.brain.storage;

import com.zuehlke.carrera.javapilot.akka.recorder.Recordable;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Date;

public abstract class RoundInformation implements Recordable{

    private int roundNr;
    private Date created;

    public RoundInformation(int roundNr) {
        this.roundNr = roundNr;
        created = new Date();
    }

    public int getRoundNr() {
        return roundNr;
    }

    public abstract boolean isEmpty();

    public abstract void add(InfoBox roundInfo);


    @Override
    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.append("roundNr", roundNr);
        json.append("created", created);
        return json;
    }

    @Override
    public Date created() {
        return created;
    }
}
