package com.zuehlke.carrera.javapilot.akka.recorder;

import com.zuehlke.carrera.javapilot.akka.brain.storage.PowerInfoBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class PowerMessageRecorder extends MessageRecorder<PowerInfoBox> {
    private static Logger logger = LoggerFactory.getLogger(PowerMessageRecorder.class);
    private final static String path = "api/power/set";
    public PowerMessageRecorder(String address){
        super(address, path);
    }

    @Override
    public void send() throws IOException {
        new Thread(() -> {
            List<PowerInfoBox> temp;
            try {
                temp = cloneAndReset();
                temp.forEach((PowerInfoBox p) -> new Thread(sendObject(p)).start());
            } catch (InterruptedException e) {
                logger.warn("Failed to send list: " + e.getMessage());
            }
            temp = null;
        }).start();
    }
    public void sendSingleObject(PowerInfoBox powerInfoBox){
        new Thread(sendObject(powerInfoBox)).start();
    }
}
