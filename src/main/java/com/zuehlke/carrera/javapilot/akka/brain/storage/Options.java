package com.zuehlke.carrera.javapilot.akka.brain.storage;

public class Options {

    //Different Info Strings
    public final static String VELOCITY_RADARS = "velocityRadars";
    public final static String CURVES = "curves";
    public final static String VELOCITY_CHECKPOINTS = "velocityCheckpoints";

    //Learnstage
    public static boolean isInLearnStage = true;

    // Defaults
    public static int DEFAULT_POWER = 130;
    public final static int DEFAULT_SLOW_DOWN_POWER = 80;
    public final static int MAX_ACCELERATION_TIME = 200;

    public static int POWER_LOW;
    public static int POWER_HIGH;
    public static int POWER_MEDIUM = 130;

    // Curves
    public static final int G_SENSOR_TOLERANCE = 1000;
    public static final int TIME_TOLERANCE_BETWEEN_DIRECTIONS = 100;

    // Radar recognition
    public static final int CHECKPOINT_ACTIVATION_DELAY = 3500;
    public static final int TIME_SINCE_PREV_RADAR_TOLERANCE = 500;

    public static final int CHECKPOINT_ACCELLERATION_STEP = 3;
    public static final int CHECKPOINT_ACCELERATION_DOWN = 5;
    public static final int MAX_ACCELLERATION_ROUNDS = 10;
    public static final int GVALUE_TOLERANCE = 500;
    // Max Gyro Value
    public static int MAX_GYRO_VALUE;

}
