package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.util.ArrayList;
import java.util.LinkedList;

public class RoundSectionInformation <T extends ConfigCheckpoint>{
    VelocityConfigCheckpoint startVelocityCheckpoint;
    VelocityConfigCheckpoint endVelocityCheckpoint;
    public final int sectionNr;

    private LinkedList<T> configCheckpoints;

    RoundSectionInformation (VelocityConfigCheckpoint start, VelocityConfigCheckpoint end, int sectionNr) {
        startVelocityCheckpoint = start;
        endVelocityCheckpoint = end;
        this.sectionNr = sectionNr;
        configCheckpoints = new LinkedList<>();
    }

    public void addConfigCheckpoint(T checkpoint) {
        configCheckpoints.add(checkpoint);
    }


    public LinkedList<T> getSectionCheckpoints() {
        return configCheckpoints;
    }
}
