package com.zuehlke.carrera.javapilot.akka.brain.InformationHolder;

public class AccelerationInformation extends GValueInformation {
    private int incrementOffset;
    private int currentPower;
    private int[] averageGyro;
    private int currentPosition;
    private final static int averageSize = 10;
    public AccelerationInformation(int currentPower, int incrementOffset) {
        super(averageSize);
        this.currentPower = currentPower;
        this.incrementOffset = incrementOffset;
        averageGyro = new int[10];
        currentPosition = 0;
    }
    public int getIncrementOffset() {
        return incrementOffset;
    }

    public void setIncrementOffset(int incrementOffset) {
        this.incrementOffset = incrementOffset;
    }

    public int getCurrentPower() {
        return currentPower;
    }

    public void setCurrentPower(int currentPower) {
        this.currentPower = currentPower;
    }
}
