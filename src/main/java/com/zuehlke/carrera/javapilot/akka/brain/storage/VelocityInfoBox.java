package com.zuehlke.carrera.javapilot.akka.brain.storage;

import java.sql.Timestamp;

public class VelocityInfoBox extends InfoBox{

    private long timePassedSinceStart;
    private long timePassedSincePrevRadar;
    private double velocity;
    private Timestamp time;

    public VelocityInfoBox(long timePassedSinceStart, double velocity, Timestamp time) {
        super(timePassedSinceStart);
        this.timePassedSinceStart = timePassedSinceStart;
        this.velocity = velocity;
        this.time = time;

        VelocityRoundInformations.lastPassedRadarTime = timePassedSinceStart;
        VelocityRoundInformations.lastMeasurement = this;
    }

    public VelocityInfoBox(long timePassedSinceStart, double velocity, Timestamp time, long timePassedSincePrevRadar) {
        this(timePassedSinceStart, velocity, time);
        this.timePassedSincePrevRadar = timePassedSincePrevRadar;
    }

    public void saveTimePassedSincePrevRadar(long timePassedSincePrevRadar){
        this.timePassedSincePrevRadar = timePassedSincePrevRadar;
    }

    public long getTimePassedSinceStartInMS() { return timePassedSinceStart; }
    public long getTimePassedSincePrevRadar() { return timePassedSincePrevRadar; }

}
