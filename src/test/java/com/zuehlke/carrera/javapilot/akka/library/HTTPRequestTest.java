package com.zuehlke.carrera.javapilot.akka.library;

import static org.junit.Assert.*;
import com.zuehlke.carrera.javapilot.akka.library.HTTPRequest;
import org.junit.Test;

public class HTTPRequestTest {
    @Test
    public void testParseUrl(){
        HTTPRequest request = new HTTPRequest("http://localhost", HTTPRequest.Method.GET);
        request.addUrlParameter("true");
        assertEquals("http://localhost/?true", request.getAddress());
    }
}
