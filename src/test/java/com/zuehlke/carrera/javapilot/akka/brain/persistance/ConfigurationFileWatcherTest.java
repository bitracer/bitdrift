package com.zuehlke.carrera.javapilot.akka.brain.persistance;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static org.junit.Assert.*;

public class ConfigurationFileWatcherTest {
    private final static String path = ConfigurationFile.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "config-watcher-test.json";
    private final ConfigurationFile config = ConfigurationFile.GetConfigurationFile(path);
    private ConfigurationFileWatcher watcher;
    @Before
    public void setUp(){
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write("{\"key1\":\"value1\", \"key2\":\"value2\"}");
            fileWriter.flush();
            fileWriter.close();
            config.loadConfiguration();
            watcher = new ConfigurationFileWatcher(config);
            watcher.start();
        } catch (IOException e){
            fail("Setup failed");
        }
    }
    @After
    public void tearDown(){
        File configuration = new File(path);
        if(configuration.exists()){
            configuration.delete();
        }
    }
    @Test
    public void testExternalWrite(){
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write("{\"key1\":\"value1\", \"key2\":\"value3\"}");
            fileWriter.flush();
            fileWriter.close();
            assertEquals("value3", config.getConfiguration("key2"));
        } catch (IOException e) {
            fail();
        }
    }
}
