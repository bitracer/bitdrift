package com.zuehlke.carrera.javapilot.akka.brain.persistance;

import com.zuehlke.carrera.javapilot.akka.brain.persistance.ConfigurationFile;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class ConfigurationFileTest {
    private ConfigurationFile configurationFile;
    private final String configurationKey = "key";
    private final String configurationValue = "value";
    private final int number = 123;
    private final static String path = ConfigurationFile.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "config-test.json";
    @Before
    public void setup(){
        configurationFile = ConfigurationFile.GetConfigurationFile(path);
    }
    @AfterClass
    public static void tearDownClass(){
        File configuration = new File(path);
        if(configuration.exists()){
            configuration.delete();
        }
    }
    @Test
    public void testAddConfigurationString(){
        configurationFile.addConfiguration(configurationKey, configurationValue);
        assertEquals(configurationValue, configurationFile.getConfiguration(configurationKey));
    }
    @Test
    public void testAddConfigurationInteger(){
        configurationFile.addConfiguration(configurationKey, Integer.toString(number));
        assertEquals(Integer.toString(number), configurationFile.getConfiguration(configurationKey));
    }
    @Test
    public void testSaveConfigurationFile(){
        configurationFile.addConfiguration(configurationKey, configurationValue);
        StringBuilder stringBuilder = new StringBuilder();
        try {
            configurationFile.writeNewFile();
            FileReader fileReader = new FileReader(path);
            while(fileReader.ready()){
                stringBuilder.append((char)fileReader.read());
            }
            fileReader.close();
        } catch (IOException e){
            fail(e.getMessage());
        }
        assertEquals("{\"key\":\"value\"}", stringBuilder.toString());
    }
    @Test
    public void testLoadConfigurationFile(){
        try{
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write("{\"key\":\"value\"}");
            fileWriter.flush();
            fileWriter.close();
            configurationFile.loadConfiguration();
        } catch (IOException e){
            fail(e.getMessage());
        }
        assertEquals(configurationValue, configurationFile.getConfiguration(configurationKey));
    }
}
