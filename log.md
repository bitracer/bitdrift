| Datum           | Zeit | Beschreibung                                                                                      | Person |
|-----------------|------|---------------------------------------------------------------------------------------------------|--------|
| 25.09.15        | 2    | Einarbeiten in AkkaFramework                                                                      | MP, OC |
| 27.09.15        | 4    | Einarbeiten in AkkaFramework                                                                      | MP,OC  |
| 01.10.15        | 4    | Recherche über Machine-Learning, Realtime Event Processing, Data Analysis and Visualization       | OC     |
| 01.10.15        | 5    | Einarbeiten in AkkaFramework und entwicklen einer Strategie zur Kurvenerkennung                   | MP     |
| 03.10.15        | 3    | Recherche über Machine-Learning, Data Analysis, Data Visualization                                | OC     |
| 03.10.15        | 4    | Verbesserung der Informationssamml-Strategie, Struktur für Informationen der Strategie aufstellen | MP     |
| 05.10.15        | 1    | Recherche für ermitteln der Geschwindigkeit über Zeit, Zentrifugalkraft und Antriebskraft         | OC     |
| 05.10.15        | 3    | Velocity-Radar- und Kurvenerkennung                                                               | MP     |
| 07.10.15        | 0,5  | Recherche für ermitteln der Geschwindigkeit über Zeit, Zentrifugalkraft und Antriebskraft         | OC     |
| 08.10.15        | 1,5  | Umstrukturierung Sourcecode, Kurvenerkennung verbessern                                           | MP     |
| 09.10.15        | 1    | Festellen der Bahn (alle Kuven inkl. Gyro-Z Value-Average)                                        | MP     |
| 14.10.15        | 2    | Implementieren von Recordable Interface um gesammelte Daten weiterzusenden und auszuwerten        | OC     |
| 15.10.15        | 0,5  | Erfassen der Requirements um Daten sinnvoll auszuwerten, fürs erste nur Kurvenerkennung auswerten | OC     |
| 17.10.15        | 1    | Implementieren von `send()` um JSON Objekt an Auswertungs-Instanz zu senden                       | OC     |
| 17.10.15        | 1    | Implementieren diverser Hilfsmethoden um alle Recorder Klassen Multithreaded zu realisieren       | OC     |
| 17.10.15        | 1    | Implementation NodeJS curveinfostore um Kurven im Broswer darzustellen                            | OC     |
| 20.10.15        | 1    | NodeJS: curve store backend mit allen wichtigen Informationen JAVA: concurrency beim senden       | OC     |
| 22.10.15        | 5    | Paralellität zum senden der Daten an Visualisierungs-instanz, Einlesen in Akka Worker Pattern     | OC     |
| 22.10.15        | 2,5  | Paralellität zum senden der Daten an Visualisierungs-instanz, Einlesen in Akka Worker Pattern     | MP     |
| 23.10.15        | 1    | NodeJS: Templating um Daten schön anzuzeigen                                                      | OC     |
| 24.10.15        | 3    | NodeJS: Abstraktion und erweiterung um zwischen Runden und Kurven beziehung herzustellen          | OC     |
| 29.10.15        | 2    | Message Boxes implementiert um ChartJS in NJS zu füttern                                          | OC     |
| 31.10.15        | 5    | NodeJS: ChartJS verwendetn um Simulator ähnliche 'Instrumente' zu schreiben                       | OC     |
| Anfang November | 20   | Motivationsfindung                                                                                | OC,MP  |
| 16.11.16        | 2    | Besprechung weiteres Vorgehen                                                                     | OC,MP  |
| 16.11.15        | 4    | Informationsbeschaffung physikalische Modelle                                                     | OC,MP  |
| 17.11.15        | 3,5  | Informationsbeschaffung physikalische Modelle                                                     | OC     |
| 19.11.15        | 2    | Recherche physikalische Modelle                                                                   | MP     |
| 21.11.15        | 7    | Versuche Gewichtserkennung für Bescheunigungsalgorithmus                                          | OC     |
| 23.11.15        | 1    | Positions-basierte Power-Funktion                                                                 | MP     |
| 24.11.15        | 2    | Positions-basierte Power-Funktion                                                                 | MP     |
| 25.11.15        | 2    | Positions-basierte Power-Funktion                                                                 | MP     |
| 25.11.15        | 2    | Datenvisualisierungsversuch mit JavaFX, Spring Web                                                | OC     |
| 26.11.15        | 5    | Datenvisualisierungsversuch mit JavaFX, Spring Web                                                | OC     |
| 28.11.15        | 6    | Datenvisualisierungsversuch mit JavaFX, Spring Web                                                | OC     |
| 28.11.15        | 4    | Entwicklung Strategie ohne Messages                                                               | MP     |
| 29.11.15        | 3    | Einarbeiten in SpringWeb                                                                          | OC     |
| 30.11.15        | 2    | Strategie auf Zeitbasis                                                                           | MP     |
| 02.12.15        | 2    | Einarbeiten in SpringWeb                                                                          | OC     |
| 03.12.15        | 3    | Entwickeln Checkpoint/Segment basierte Strategie                                                  | MP     |
| 04.12.15        | 2    | Recherche Beschleunigungsalgorithmen                                                              | OC     |
| 05.12.15        | 2    | Beschleunigungsalgorithmus                                                                        | OC     |
| 07.12.15        | 1    | Verfeinerung Beschleunigungsalgorithmus                                                           | OC     |
| 08.12.15        | 1    | Checkpoint/Segmentbasierte Strategie                                                              | MP     |
| 09.12.15        | 2    | Einlesen in JavaFX                                                                                | OC     |
| 10.12.15        | 2    | Einlesen in JavaFX                                                                                | OC     |
| 11.12.15        | 5    | Datenvisualisierung mit JavaFX                                                                    | OC     |
| 11.12.15        | 3    | FileWatcher und ConfigurationFile für 2. Trainingsession                                          | OC     |
| 12.12.15        | 8    | Datenvisualisierung mit JavaFX und FileWatcher                                                    | OC     |
| 13.12.15        | 5    | Checkpoint/Segmentbasierte Strategie                                                              | MP     |
| 18.12.15        | 4    | Datenvisualisierung mit JavaFX                                                                    | OC     |
| 18.12.15        | 0    | JavaFX verworfen                                                                                  | OC     |
| 18.12.15        | 10   | Checkpoint/Segmentbasierte Strategie                                                              | MP     |
| 19.12.15        | 10,5 | Checkpoint/Segmentbasierte Strategie                                                              | MP     |
| 20.12.15        | 2    | Beschleunigungsalgorithmus wieder implementiert                                                   | OC     |
| 21.12.15        | 3    | Checkpoint/Segmentbasierte Strategie und Beschleunigungs Algorithmus                              | OC     |
| 22.12.15        | 1    | Checkpoint/Segmentbasierte Strategie                                                              | OC     |
| 28.12.15        | 3    | FileWatcher und Options File                                                                      | OC     |
| 29.12.15        | 2    | Besprechung weiteres Vorgehen                                                                     | OC,MP  |
| 02.01.16        | 2    | Dokumentation                                                                                     | OC     |
| 03.01.16        | 2    | Aufräumen                                                                                         | OC     |
| 04.01.16        | 2    | Implementation Beschleunigungsalgorithmus mit Checkpoints                                         | MP,OC  |
| 05.01.16        | 2    | Checkpoint/Segmentbasierte Strategie                                                              | OC,MP  |
| 06.01.16        | 1    | Dokumentation                                                                                     | OC     |
| 09.01.15        | 4    | Aufräumen und Dokumentieren                                                                       | OC, MP |
